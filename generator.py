# -*- coding: utf-8 -*-
import os
#from typing import ChainMap

class ProjectGenerator():
    def __init__(self) -> int:
        super().__init__()
        print("Script to create a C/C++/CUDA project and init it with basic CMakeLists.txt configuration")
        self.ProjectLocation = input("Where do you want to generate your project ? (Folder must already exist !)\n")
        self.ProjectName = input("What's the name of your project ?\n")
        self.ProjectDesc = input("What's your project {}'s decription\n".format(self.ProjectName))
        self.ProjectAutor = input("What's your name ?\n")
        self.ProjectMail = input("What's your mail ?\n")

        # Project type (C++ or CUDA)
        while True:
            try:
                self.ProjectType = eval(input("Do you wish to create a C/C++ Project (1) or CUDA Project (2) ? "))
            except ValueError:
                print("Invalid input, please enter 1 or 2 according to your choice")
                continue
            if self.ProjectType not in [1,2]:
                print("Invalid input, please enter 1 or 2 according to your choice")
            else :
                break
        
        # CStandard version
        print("Wich standard version of C you want to use ? ")
        while True:
            try:
                self.CStandard = input("Please enter 99, 11, 14, 17 or 20 : ")
            except ValueError:
                print("Invalid input, please enter 99, 11, 14, 17 or 20 according to your choice")
                continue
            if self.CStandard not in ('99','11','14','17','20'):
                print("Invalid input, please enter 99, 11, 14, 17 or 20 according to your choice")
            else :
                break
        
        if self.ProjectType == 2:
        # CUDA version
            print("Wich standard version of CUDA you want to use ? ")
            while True:
                try:
                    self.CUDAStandard = input("Please enter 99, 11, 14 or 17 : ")
                except ValueError:
                    print("Invalid input, please enter 99, 11, 14 or 17 according to your choice")
                    continue
                if self.CUDAStandard not in ('99','11','14','17'):
                    print("Invalid input, please enter 99, 11, 14 or 17 according to your choice")
                else :
                    break
            
            # CUDA Arch
            self.ProjectCUDAaarch = input("What's your GPU architecture ? (ex : 53, 75, etc) : ")

        #Libraries
        print("What are the library you need ?")
        print("Please write the correct library name & if multiple, seperate with |")
        print("If a library is requiered please add _R at the end")
        print("Example : Eigen3|OpenCV_R")
        self.ProjectLibs = input("Libraries :\n")

        #OpenGL
        while True:
            try:
                self.UseOpenGL = input("Do you wish to add OpenGL support to your project ? ")
            except ValueError:
                print("Invalid input, please yes,no,y or n according to your choice")
                continue
            if self.UseOpenGL not in ('yes','no','y','n'):
                print("Invalid input, please yes,no,y or n according to your choice")
                continue
            if self.UseOpenGL in ('no','n'):
                #print("Project will not use OpenGL support")
                break

        #Generate ?
        print('='*60)
        print("Project will be named {}".format(self.ProjectName))
        print("Project will use C Standard version {}".format(self.CStandard))
        print("Your project will be generated in folder \n{}".format(self.ProjectLocation))
        if self.ProjectType == 2:
            print("Project is CUDA based")
            print("Project will use CUDA Standard version {}".format(self.CUDAStandard))
        print(f'''Project will{" not" if self.UseOpenGL in ('no','n') else ""} use OpenGL support''')
        print('='*60)

        while True:
            try:
                self.ProjectGenerate = input("Do you wish to continue and generate project ? ")
            except ValueError:
                print("Invalid input, please yes,no,y or n according to your choice")
                continue
            if self.ProjectGenerate not in ('yes','no','y','n'):
                print("Invalid input, please yes,no,y or n according to your choice")
                continue
            if self.ProjectGenerate in ('no','n'):
                print("Generator Process stopped by user... Exiting")
                break
            else :
                break
        return 
    
    def filesGenerator(self):
        if self.ProjectGenerate not in ('yes','y'):
            return 
        print("Generating all files which should be written")

        os.mkdir(self.ProjectLocation+'/cmake')
        os.mkdir(self.ProjectLocation+'/src')
        os.mkdir(self.ProjectLocation+'/tests')

        CMakeListsRoot = f"""
# ----------------------------------------------------------------------------
#   Basic Configuration
# ----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
"""
        if self.ProjectType == 2:
            CMakeListsRoot+= f"""
project({self.ProjectName.upper()} VERSION "0.1" LANGUAGES CXX CUDA)"""
        else :
            CMakeListsRoot+= f"""
project({self.ProjectName.upper()} VERSION "0.1" LANGUAGES CXX )"""

        CMakeListsRoot += """
set(PROJECT_SOVERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}")
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD $CStandard) # C/C++$CStandard...
set(CMAKE_CXX_STANDARD_REQUIRED ON) #...is required...
set(CMAKE_CXX_EXTENSIONS ON) #...with compiler extensions like gnu++$CStandard

include(cmake/options.cmake)

include(cmake/findDependencies.cmake)
include(cmake/compilerOptions.cmake)



add_subdirectory(src)

if(BUILD_TESTS)
  add_subdirectory(tests)
endif()

include(cmake/printInfo.cmake)
include(cmake/installOptions.cmake)

# Package Generator  #######################################################
IF(BUILD_DEBPACKAGE)
include(cmake/cpack.cmake)
ENDIF()
"""

        CMakeIN_Uninstall = """
# -----------------------------------------------
# File that provides "make uninstall" target
#  We use the file 'install_manifest.txt'
# -----------------------------------------------
IF(NOT EXISTS "@CMAKE_CURRENT_BINARY_DIR@/install_manifest.txt")
  MESSAGE(FATAL_ERROR "Cannot find install manifest: \"@CMAKE_CURRENT_BINARY_DIR@/install_manifest.txt\"")
ENDIF(NOT EXISTS "@CMAKE_CURRENT_BINARY_DIR@/install_manifest.txt")

FILE(READ "@CMAKE_CURRENT_BINARY_DIR@/install_manifest.txt" files)
STRING(REGEX REPLACE "\\n" ";" files "${files}")
FOREACH(file ${files})
  MESSAGE(STATUS "Uninstalling \"$ENV{DESTDIR}${file}\"")
#  IF(EXISTS "$ENV{DESTDIR}${file}")
#    EXEC_PROGRAM(
#      "@CMAKE_COMMAND@" ARGS "-E remove \"$ENV{DESTDIR}${file}\""
#      OUTPUT_VARIABLE rm_out
#      RETURN_VALUE rm_retval
#      )
	EXECUTE_PROCESS(COMMAND rm $ENV{DESTDIR}${file})
#    IF(NOT "${rm_retval}" STREQUAL 0)
#      MESSAGE(FATAL_ERROR "Problem when removing \"$ENV{DESTDIR}${file}\"")
#    ENDIF(NOT "${rm_retval}" STREQUAL 0)
#  ELSE(EXISTS "$ENV{DESTDIR}${file}")
#    MESSAGE(STATUS "File \"$ENV{DESTDIR}${file}\" does not exist.")
#  ENDIF(EXISTS "$ENV{DESTDIR}${file}")
ENDFOREACH(file)

"""

        CMakeCompilerOptions = """


IF(NOT TARGET_PROCESSOR )
    SET(TARGET_PROCESSOR ${CMAKE_SYSTEM_PROCESSOR})
ENDIF()

IF(CMAKE_COMPILER_IS_GNUCXX OR MINGW OR  (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )

    if(${TARGET_PROCESSOR} MATCHES armv7l) # In ARM_COrtex8 with neon, enalble vectorized operations
        set(GENERAL_FLAGS "${GENERAL_FLAGS} -mcpu=cortex-a8 -mfpu=neon -mfloat-abi=hard ")
        SET(ADD_LINK_LIBS -latomic)#for raspian, Opencv has not included it
    endif()
    if(${TARGET_PROCESSOR} MATCHES armv6l) # In ARM_COrtex8 with neon, enalble vectorized operations
        set(GENERAL_FLAGS "${GENERAL_FLAGS}  -mabi=aapcs-linux -marm  -march=armv6 -mfloat-abi=hard  -mfp16-format=none -mfpu=vfp -mlittle-endian -mpic-data-is-text-relative -mrestrict-it -msched-prolog -mstructure-size-boundary=0x20 -mtp=auto -mtls-dialect=gnu -munaligned-access -mvectorize-with-neon-quad")
        SET(ADD_LINK_LIBS -latomic)#for raspian, Opencv has not included it
    endif()


    SET(CMAKE_CXX_FLAGS_RELEASE         "${CMAKE_CXX_FLAGS} ${GENERAL_FLAGS}  -O3 -g0  -DNDEBUG")
    SET(CMAKE_CXX_FLAGS_DEBUG           "${CMAKE_CXX_FLAGS} ${GENERAL_FLAGS}  -O0 -g3  -DDEBUG -D_DEBUG -DPRINT_DEBUG_MESSAGES")
    SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO  "${CMAKE_CXX_FLAGS} ${GENERAL_FLAGS}  -O1 -g3  -D_DEBUG -DDEBUG -DPRINT_DEBUG_MESSAGES")


ELSE()  # MSVC
    ADD_DEFINITIONS(-DNOMINMAX)
ENDIF()#END OF COMPILER SPECIFIC OPTIONS



SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${EXTRA_EXE_LINKER_FLAGS} ${ADD_LINK_LIBS}")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${EXTRA_EXE_LINKER_FLAGS_RELEASE}")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} ${EXTRA_EXE_LINKER_FLAGS_DEBUG}")

"""

        CMakeConfig = """
# ===================================================================================
#  @PROJECT_NAME@ CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project:
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(@PROJECT_NAME@ REQUIRED )
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME ${@PROJECT_NAME@_LIBS})
#
#    This file will define the following variables:
#      - @PROJECT_NAME@_LIBS          : The list of libraries to links against.
#      - @PROJECT_NAME@_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                with this path is NOT needed.
#      - @PROJECT_NAME@_VERSION       : The  version of this PROJECT_NAME build. Example: "1.2.0"
#      - @PROJECT_NAME@_VERSION_MAJOR : Major version part of VERSION. Example: "1"
#      - @PROJECT_NAME@_VERSION_MINOR : Minor version part of VERSION. Example: "2"
#      - @PROJECT_NAME@_VERSION_PATCH : Patch version part of VERSION. Example: "0"
#
# ===================================================================================
INCLUDE_DIRECTORIES("@CMAKE_INSTALL_PREFIX@/include")
INCLUDE_DIRECTORIES("@CMAKE_INSTALL_PREFIX@/include/@PROJECT_NAME@")
SET(@PROJECT_NAME@_INCLUDE_DIRS "@CMAKE_INSTALL_PREFIX@/include")

LINK_DIRECTORIES("@CMAKE_INSTALL_PREFIX@/lib")
SET(@PROJECT_NAME@_LIB_DIR "@CMAKE_INSTALL_PREFIX@/lib")

SET(@PROJECT_NAME@_LIBS @OpenCV_LIBS@ @PROJECT_NAME@@PROJECT_DLLVERSION@) 

SET(@PROJECT_NAME@_FOUND YES)
SET(@PROJECT_NAME@_FOUND "YES")
SET(@PROJECT_NAME@_VERSION        @PROJECT_VERSION@)
SET(@PROJECT_NAME@_VERSION_MAJOR  @PROJECT_VERSION_MAJOR@)
SET(@PROJECT_NAME@_VERSION_MINOR  @PROJECT_VERSION_MINOR@)
SET(@PROJECT_NAME@_VERSION_PATCH  @PROJECT_VERSION_PATCH@)

"""

        CMakeCpack = f"""
set(CPACK_PACKAGE_DESCRIPTION "{self.ProjectName.upper()} Package")
set(CPACK_PACKAGE_VERSION_MAJOR "${{PROJECT_VERSION_MAJOR}}")
set(CPACK_PACKAGE_VERSION_MINOR "${{PROJECT_VERSION_MINOR}}")
set(CPACK_PACKAGE_VERSION_PATCH "${{PROJECT_VERSION_PATCH}}")

set(CPACK_PACKAGE_VENDOR "{self.ProjectAutor}")
set(CPACK_PACKAGE_CONTACT "{self.ProjectMail}")

set(CPACK_PACKAGE_FILE_NAME "${{CMAKE_PROJECT_NAME}}-${{PROJECT_VERSION_MAJOR}}.${{PROJECT_VERSION_MINOR}}.${{PROJECT_VERSION_PATCH}}")

SET(CPACK_PACKAGE_DESCRIPTION "{self.ProjectName.upper()} library and applications")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "{self.ProjectDesc}")

SET(CPACK_GENERATOR "DEB")
SET(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "i386")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS " ")
SET(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")

include (CPack)
"""

        CMakeFindDependencies = """
# ----------------------------------------------------------------------------
#   Find Dependencies
# ----------------------------------------------------------------------------
"""

        if self.ProjectLibs != "":
            self.Libs = self.ProjectLibs.split('|')
            for i in self.Libs:
                if '_R' in i:
                    #print(i.split('_')[0],"Package is required !")
                    CMakeFindDependencies+=f'''find_package({i.split('_')[0]} REQUIRED)\ninclude_directories(${{{i.split('_')[0]}_INCLUDE_DIRS}})'''
                else:
                    CMakeFindDependencies+=f'''find_package({i} REQUIRED)\ninclude_directories(${{{i}_INCLUDE_DIRS}})'''
        else :
            print("No default libs specified...")

        if self.UseOpenGL in ('yes','y'):
            CMakeFindDependencies+="""
if(EXISTS ${GLUT_PATH})
	include_directories(${GLUT_PATH}/include)
	set(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} ${GLUT_PATH}/lib)
endif()

##LOOK FOR OPENGL AND GLUT
#FIND OPENGL LIBRARY. In Linux, there is no need since it is included
if(NOT ANDROID_CREATION)
	if(BUILD_GLSAMPLES)
		find_package(OpenGL)
		find_package(GLUT)#standard package
		message(STATUS "GLUT_FOUND=${GLUT_FOUND} OPENGL_gl_LIBRARY=${OPENGL_gl_LIBRARY} GLUT_HEADER=${GLUT_HEADER}")
	endif()

	if(NOT GLUT_FOUND) #else, freeglut
		find_library(GLUT_glut_LIBRARY     NAMES freeglut)
		message(STATUS "GLUT_glut_LIBRARY=${GLUT_glut_LIBRARY}")
	endif()

	if ( (NOT GLUT_glut_LIBRARY AND NOT GLUT_FOUND) OR NOT OPENGL_gl_LIBRARY)
		set(GL_FOUND "NO")
	else()
		set(GL_FOUND "YES")
		set (OPENGL_LIBS  ${OPENGL_gl_LIBRARY} ${OPENGL_glu_LIBRARY} ${GLUT_glut_LIBRARY})
	endif()
endif()
"""

        CMakeInstallOptions = f'''
# pkg-config
configure_file(${{PROJECT_SOURCE_DIR}}/cmake/{self.ProjectName.lower()}.pc.in {self.ProjectName.lower()}.pc @ONLY)
configure_file(${{PROJECT_SOURCE_DIR}}/cmake/{self.ProjectName.lower()}-uninstalled.pc.in {self.ProjectName.lower()}-uninstalled.pc @ONLY)
install(FILES "${{PROJECT_BINARY_DIR}}/{self.ProjectName.lower()}-uninstalled.pc" "${{PROJECT_BINARY_DIR}}/{self.ProjectName.lower()}.pc" DESTINATION lib/pkgconfig)

CONFIGURE_FILE("${{PROJECT_SOURCE_DIR}}/cmake/config.cmake.in" "${{PROJECT_BINARY_DIR}}/${{PROJECT_NAME}}Config.cmake")
INSTALL(FILES "${{PROJECT_BINARY_DIR}}/${{PROJECT_NAME}}Config.cmake" DESTINATION share/${{PROJECT_NAME}} )
if(WIN32)
    SET(PROJECT_DLLVERSION "${{PROJECT_VERSION_MAJOR}}${{PROJECT_VERSION_MINOR}}${{PROJECT_VERSION_PATCH}}")
ENDIF()
'''

        ProjectUninstall = f"""
libdir=@CMAKE_CURRENT_BINARY_DIR@
includedir=@CMAKE_CURRENT_SOURCE_DIR@/src

Name: @PROJECT_NAME@
Description: {self.ProjectDesc} Uninstalled Version.
Version: @PROJECT_VERSION@
Requires: 
Libs: -L${{libdir}} -l{self.ProjectName.lower()}
Cflags: -I${{includedir}}

"""

        ProjectInstall = f"""
# pkg-config file for the library

prefix=@CMAKE_INSTALL_PREFIX@
exec_prefix=${{prefix}}
libdir=${{exec_prefix}}/lib
includedir=${{prefix}}/include

Name: @PROJECT_NAME@
Description: {self.ProjectDesc}
Version:  @PROJECT_VERSION@
Requires: 
Conflicts: 
Libs: -L${{libdir}} -l@PROJECT_NAME@
Cflags: -I${{includedir}}/@PROJECT_NAME@ -I${{includedir}}

"""

        CMakeOptions = '''
#------------------------------------------------------
# Build type
#------------------------------------------------------

if(NOT CMAKE_BUILD_TYPE )
   set( CMAKE_BUILD_TYPE "Debug" )
endif()

#------------------------------------------------------
# Lib Names and Dirs
#------------------------------------------------------

if(WIN32)
    # Postfix of DLLs:
    # Postfix of DLLs:
    set(PROJECT_DLLVERSION "${PROJECT_VERSION_MAJOR}${PROJECT_VERSION_MINOR}${PROJECT_VERSION_PATCH}")
    set(RUNTIME_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin CACHE PATH "Directory for dlls and binaries")
    set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin CACHE PATH "Directory for binaries")
    set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin CACHE PATH "Directory for dlls")

else()
    # Postfix of so's:
    set(PROJECT_DLLVERSION)
#    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_INSTALL_PREFIX}/lib/cmake/ /usr/lib/cmake)
endif()

option(BUILD_TESTS	"Set to OFF to not compile utils " ON)
option(BUILD_SHARED_LIBS 	"Set to OFF to build static libraries" ON)
option(BUILD_GLSAMPLES 	"Set to OFF to build static libraries" OFF)
option(USE_TIMERS 	"Set to OFF to build static libraries" OFF)
option(BUILD_DEBPACKAGE 	"Set to ON to use cpack" OFF)
option('''+f'''{self.ProjectName.upper()}_DEVINSTALL'''+''' 	"Set to OFF to disable source installation" ON)
  iF(USE_TIMERS)
add_definitions(-DUSE_TIMERS)
ENDIF()

# ----------------------------------------------------------------------------
#   Uninstall target, for "make uninstall"
# ----------------------------------------------------------------------------
configure_file("${PROJECT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in" "${PROJECT_BINARY_DIR}/cmake_uninstall.cmake" IMMEDIATE @ONLY)
add_custom_target(uninstall "${CMAKE_COMMAND}" -P "${PROJECT_BINARY_DIR}/cmake_uninstall.cmake")

'''

        CMakeInfos =  '''
# ----------------------------------------------------------------------------
# display status message for important variables
# ----------------------------------------------------------------------------
message( STATUS )
message( STATUS "-------------------------------------------------------------------------------" )
message( STATUS "General configuration for ${PROJECT_NAME} ${PROJECT_VERSION}")
message( STATUS "-------------------------------------------------------------------------------" )
message( STATUS )
message( STATUS "Compiler:"                   "${CMAKE_COMPILER}"   "${CMAKE_CXX_COMPILER}")
message( STATUS "FOUND CUDA Include DIR      ${CUDA_INCLUDE_DIRS}")
message( STATUS "C++ flags (Release):       ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE}")
message( STATUS "C++ flags (Debug):         ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG}")
message( STATUS "CMAKE_CXX_FLAGS:         ${CMAKE_CXX_FLAGS}")
message( STATUS "CMAKE_BINARY_DIR:         ${CMAKE_BINARY_DIR}")

message( STATUS )
message( STATUS "TARGET_PROCESSOR = ${TARGET_PROCESSOR}" )
message( STATUS "BUILD_SHARED_LIBS = ${BUILD_SHARED_LIBS}" )
message( STATUS "CMAKE_INSTALL_PREFIX = ${CMAKE_INSTALL_PREFIX}" )
message( STATUS "CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}" )
message( STATUS )

message( STATUS "BUILD_SVM = ${BUILD_SVM}" )
message( STATUS "BUILD_TESTS = ${BUILD_TESTS}" )
message( STATUS "BUILD_GLSAMPLES = ${BUILD_GLSAMPLES}" )

message( STATUS "CMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}")
message( STATUS "USE_TIMERS=${USE_TIMERS}")

message( STATUS )
message( STATUS "---------------------------     LIBS     -----------------------------" )
message( STATUS )
'''

        if self.ProjectLibs != "":
            self.Libs = self.ProjectLibs.split('|')
            for i in self.Libs:
                if '_R' in i:
                    CMakeInfos+=f'''message( STATUS "{i.split('_')[0]}_LIB_DIR=${{{i.split('_')[0]}_LIB_DIR}} {i.split('_')[0]}_INCLUDE_DIRS=${{{i.split('_')[0]}_INCLUDE_DIRS}}")'''
                else:
                    CMakeInfos+=f'''message( STATUS "{i}_LIB_DIR=${{{i}_LIB_DIR}} {i}_INCLUDE_DIRS=${{{i}_INCLUDE_DIRS}}")'''
                    
        CMakeListsSrc = f'''
SET (LIBNAME ${{EXTRALIBNAME}}{self.ProjectName.lower()})
include_directories(.)'''

        if self.ProjectType == 2:
            CMakeListsSrc += f'''
find_package(CUDA  REQUIRED)
include_directories("${{CUDA_INCLUDE_DIRS}}")
SET (CMAKE_CUDA_ARCHITECTURES {self.ProjectCUDAaarch})
'''
        CMakeListsSrc += f'''
set(CMAKE_CXX_STANDARD {self.CStandard})
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
'''
        if self.ProjectType == 2:
            CMakeListsSrc+=f'''
set(CMAKE_CUDA_STANDARD {self.CUDAStandard})
set(CMAKE_CUDA_STANDARD_REQUIRED TRUE)
set(CMAKE_CUDA_EXTENSIONS ON)
enable_language(CUDA)'''

        CMakeListsSrc+=f'''
get_property(LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)


SET(sources
    {self.ProjectName.lower()}.cpp
    )
SET(headers
    {self.ProjectName.lower()}.h
    )

add_library(${{LIBNAME}} ${{sources}} ${{headers}})
 
set_target_properties(${{LIBNAME}} PROPERTIES          # create *nix style library versions + symbolic links
    DEFINE_SYMBOL {self.ProjectName.upper()}_DSO_EXPORTS
    VERSION ${{PROJECT_VERSION}}
    SOVERSION ${{PROJECT_SOVERSION}}
    CLEAN_DIRECT_OUTPUT 1                       # allow creating static and shared libs without conflicts
    OUTPUT_NAME "${{LIBNAME}}${{PROJECT_DLLVERSION}}"    # avoid conflicts between library and binary target names
    INTERFACE_COMPILE_FEATURES "cxx_std_{self.CStandard}"
)

target_link_libraries(${{LIBNAME}} PUBLIC )
'''

        if self.ProjectType == 2:
            CMakeListsSrc += '''
target_link_libraries(${LIBNAME} PRIVATE cuda )
'''
        else :
            CMakeListsSrc += '''
target_link_libraries(${LIBNAME} PRIVATE )
'''
        CMakeListsSrc += f'''
INSTALL(TARGETS ${{LIBNAME}}
    RUNTIME DESTINATION bin COMPONENT main			# Install the dll file in bin directory
    LIBRARY DESTINATION lib PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE COMPONENT main
    ARCHIVE DESTINATION lib COMPONENT main)			# Install the dll.a file in lib directory

IF({self.ProjectName.upper()}_DEVINSTALL)
   install(FILES ${{headers}}  DESTINATION include/{self.ProjectName.lower()})
ENDIF()

'''

        CMakeListsTests = '''
INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/src)
'''
        if self.ProjectType == 2:
            CMakeListsTests+=f'''
find_package(CUDA  REQUIRED)
include_directories("${{CUDA_INCLUDE_DIRS}}")

SET (CMAKE_CUDA_ARCHITECTURES {self.CUDAStandard})
'''
        CMakeListsTests += f'''
set(CMAKE_CXX_STANDARD {self.CStandard})
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
'''
        if self.ProjectType == 2:
            CMakeListsTests+=f'''
set(CMAKE_CUDA_STANDARD {self.CUDAStandard})
set(CMAKE_CUDA_STANDARD_REQUIRED TRUE)
set(CMAKE_CUDA_EXTENSIONS ON)
enable_language(CUDA)
'''
        CMakeListsTests+=f'''
get_property(LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)

if(CMAKE_COMPILER_IS_GNUCXX OR MINGW OR ${{CMAKE_CXX_COMPILER_ID}} STREQUAL Clang)
SET(THREADLIB "pthread")
ENDIF()
'''
        if self.ProjectType == 2:
            CMakeListsTests+=f'''
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    target_compile_options({self.ProjectName}_test PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-G>)
endif()
'''
            CMakeListsTests+=f'''
add_executable({self.ProjectName}_test {self.ProjectName}_test.cu)
'''
        else :
            CMakeListsTests+=f'''
add_executable({self.ProjectName}_test {self.ProjectName}_test.cpp)
'''
        CMakeListsTests +=f'''
target_link_libraries({self.ProjectName}_test {self.ProjectName.lower()} ${{THREADLIB}})

INSTALL(TARGETS {self.ProjectName}_test RUNTIME DESTINATION bin)

'''

        with open(self.ProjectLocation+'/CMakeLists.txt','w') as writer:
            writer.write(CMakeListsRoot)
            writer.close()
        
        with open(self.ProjectLocation+'/cmake/cmake_uninstall.cmake.in','w') as writer:
            writer.write(CMakeIN_Uninstall)
            writer.close()
        
        with open(self.ProjectLocation+'/cmake/compilerOptions.cmake','w') as writer:
            writer.write(CMakeCompilerOptions)
            writer.close()

        with open(self.ProjectLocation+'/cmake/config.cmake.in','w') as writer:
            writer.write(CMakeConfig)
            writer.close()

        with open(self.ProjectLocation+'/cmake/cpack.cmake','w') as writer:
            writer.write(CMakeCpack)
            writer.close()
        
        with open(self.ProjectLocation+'/cmake/findDependencies.cmake','w') as writer:
            writer.write(CMakeFindDependencies)
            writer.close()

        with open(self.ProjectLocation+'/cmake/installOptions.cmake','w') as writer:
            writer.write(CMakeInstallOptions)
            writer.close()

        with open(self.ProjectLocation+f'/cmake/{self.ProjectName.lower()}-uninstalled.pc.in','w') as writer:
            writer.write(ProjectUninstall)
            writer.close()
        
        with open(self.ProjectLocation+f'/cmake/{self.ProjectName.lower()}.pc.in','w') as writer:
            writer.write(ProjectInstall)
            writer.close()

        with open(self.ProjectLocation+'/cmake/options.cmake','w') as writer:
            writer.write(CMakeOptions)
            writer.close()

        with open(self.ProjectLocation+'/cmake/printInfo.cmake','w') as writer:
            writer.write(CMakeInfos)
            writer.close()

        with open(self.ProjectLocation+'/src/CMakeLists.txt','w') as writer:
            writer.write(CMakeListsSrc)
            writer.close()
        
        with open(self.ProjectLocation+f'/src/{self.ProjectName.lower()}.cpp','w') as writer:
            writer.write('\n')
            writer.close()
        
        with open(self.ProjectLocation+f'/src/{self.ProjectName.lower()}.h','w') as writer:
            writer.write('\n')
            writer.close()

        if self.ProjectType == 2:
            with open(self.ProjectLocation+f'/tests/{self.ProjectName}_test.cu','w') as writer:
                writer.write('\n')
                writer.close()
        else :
            with open(self.ProjectLocation+f'/tests/{self.ProjectName}_test.cpp','w') as writer:
                writer.write('\n')
                writer.close()

        with open(self.ProjectLocation+'/tests/CMakeLists.txt','w') as writer:
            writer.write(CMakeListsTests)
            writer.close()
        

if __name__ == "__main__":
    try:
        Gen = ProjectGenerator()
        Gen.filesGenerator()
    except KeyboardInterrupt:
        print("\nGenerator Process stopped by user... Exiting")
